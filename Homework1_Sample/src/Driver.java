
public class Driver {
	
	public static void main(String [] args){
		Driver driver = new Driver();
		driver.driveCar("Ford");
		driver.driveTruck("Chevy");
	}
	
	public void driveCar(String brand){
		Car car = new Car(brand);
		car.start();
		car.turn();
		car.stop();
		car.openTrunk();
	    car.closeTrunk();
	}
	
	public void driveTruck(String brand){
		Truck truck = new Truck(brand);
		truck.start();
		truck.openTailgate();
		truck.turn();
		truck.stop();
		truck.closeTailgate();
	}
	
}
