
public class Truck extends Automobile {
	
	public Truck(){
		this.setBodyStyle("Truck");
		this.setBrand("Unknown Brand");
	}
	
	public Truck(String brand){
		this.setBodyStyle("Truck");
		this.setBrand(brand);
	}
	
	public void openTailgate(){
		System.out.println("Opened the tailgate of the " + this.getBrand() + " " + this.getBodyStyle());
	}
	
	public void closeTailgate(){
		System.out.println("Closed the tailgate of the " + this.getBrand() + " " + this.getBodyStyle());
	}
}
