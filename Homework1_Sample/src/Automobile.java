
 public abstract class Automobile{
	private String brand;
	private String bodyStyle;
	

	public Automobile(){
		brand = "Default Brand";
	}
	
	public void start(){
		System.out.println("Starting " + brand + " " + bodyStyle);
	}
	
	public void stop(){
		System.out.println("Stopping " + brand + " " + bodyStyle);
	}
	
	public void turn(){
		System.out.println(brand + " " + bodyStyle + " is turning");
	}
	
	public String getBrand(){
		return brand;
	}
	
	public void setBrand(String brand){
		this.brand = brand;
	}
	
	public String getBodyStyle(){
		return bodyStyle;
	}
	
	public void setBodyStyle(String bodyStyle){
		this.bodyStyle = bodyStyle;
	}

	
	}
	

